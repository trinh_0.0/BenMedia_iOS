//
//  ImageCell.swift
//  BenMedia
//
//  Created by Đỗ Ngọc Trình on 1/15/18.
//  Copyright © 2018 Đỗ Ngọc Trình. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var imvContent: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }

}
