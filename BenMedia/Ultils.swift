//
//  Ultils.swift
//  BenMedia
//
//  Created by Đỗ Ngọc Trình on 1/21/18.
//  Copyright © 2018 Đỗ Ngọc Trình. All rights reserved.
//

import Foundation
import UIKit

class Random {
    
    subscript<T>(_ min: T, _ max: T) -> T where T : BinaryInteger {
        get {
            return rand(min-1, max+1)
        }
    }
}

let rand = Random()

func rand<T>(_ min: T, _ max: T) -> T where T : BinaryInteger {
    let _min = min + 1
    let difference = max - _min
    return T(arc4random_uniform(UInt32(difference))) + _min
}

func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
    //let fontSize = UIFont.systemFontSize
    let attrs = [
        NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 31),
        //NSAttributedStringKey.foregroundColor: UIColor.lightGray
    ]
    let nonBoldAttribute = [
        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 28),
        ]
    let attrStr = NSMutableAttributedString(string: string, attributes: attrs)
    if let range = nonBoldRange {
        attrStr.setAttributes(nonBoldAttribute, range: range)
    }
    return attrStr
}
