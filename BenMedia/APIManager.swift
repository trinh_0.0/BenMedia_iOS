//
//  APIManager.swift
//  BenMedia
//
//  Created by Đỗ Ngọc Trình on 1/15/18.
//  Copyright © 2018 Đỗ Ngọc Trình. All rights reserved.
//

import UIKit
import Alamofire

struct Media: Codable {
    let urlThumnail : String
    let urlImage: String
    let info: String
    init(dictionary: [String: Any]) {
        self.info = dictionary["info"] as? String ?? ""
        self.urlImage = dictionary["urlImage"] as? String ?? ""
        self.urlThumnail = dictionary["urlThumnail"] as? String ?? ""
    }
}

class APIManager: NSObject {
    static func getJSON(url: String, completion: @escaping ([Media]?) -> Void){
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                if let json = response.result.value {
                    let jsonObject = json as? Dictionary<String, Any>
                    let collection = jsonObject!["collection"] as? Array<Any>
                    if collection != nil {
                        var products = [Media]()
                        for jsonItem in collection! {
                            if jsonItem is Dictionary<String, Any>
                            {
                                let product = Media.init(dictionary: jsonItem as! [String : Any])
                                if !product.urlImage.isEmpty
                                {
                                    products.append(product)
                                }
                                
                            }
                        }
                        completion(products)
                    }
                    else {
                        completion(nil)
                    }
                }
                completion(nil)
        }
    }
}
